package com.leyou.item.mapper;

import com.leyou.item.pojo.SpecGroup;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author HongBo Wang
 * @data 2018/6/27 10:51
 */
public interface SpecGroupMapper extends Mapper<SpecGroup> {

}
